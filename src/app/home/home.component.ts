import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  length: number;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  storedImages = [];
  itemsToShow = [];
  searchStr = '';

  constructor(
    private http: HttpClient
    ) { }

  ngOnInit() {
    this.getImages();
    this.paginator.pageIndex = 0;
  }

  getImages(): void {
    this.http.get('http://jsonplaceholder.typicode.com/photos')
        .subscribe((response: any) => {
          this.storedImages = response;
          this.showImages(0, this.pageSize);
        });
  }

  showData() {
    let showThisItems: any;
    if (this.searchStr.length) {
      showThisItems = this.storedImages.filter(x => x.title.toLowerCase().includes(this.searchStr.toLowerCase()));
    } else {
      showThisItems = this.storedImages;
    }
    this.length =  showThisItems.length;
    return showThisItems;
  }

  showImages(pageIndex: number, pageSize: number, resetPaginator?: boolean) {
    if (resetPaginator) {
      this.paginator.pageIndex = 0;
    }
    this.itemsToShow = this.showData().slice(pageIndex * pageSize, (pageIndex + 1) * pageSize);
  }

  onPaginateChange(event: any): void {
    this.showImages(event.pageIndex, event.pageSize);
  }
}
